// Import Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

// Import Services
import { SocketService } from './services/socket.service'

// Import Components
import { AppComponent } from './app.component';
import { TemperatureComponent } from './temperature/temperature.component';
import { HomeComponent } from './home/home.component';

// Routing
const routes = [
  { path: '', component: HomeComponent },
  { path: 'temperature', component: TemperatureComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    TemperatureComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    SocketService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
