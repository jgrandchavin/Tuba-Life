import { Injectable } from '@angular/core';


import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import * as io from 'socket.io-client';

@Injectable()
export class SocketService {

  private BACKEND_URL = 'http://localhost:3000';
  private socket;

  constructor() { }

  getTemperature() {
    let observable = new Observable(observer => {
      this.socket = io(this.BACKEND_URL);
      this.socket.on('temperature', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  getHumidity() {
    let observable = new Observable(observer => {
      this.socket = io(this.BACKEND_URL);
      this.socket.on('humidity', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

}
