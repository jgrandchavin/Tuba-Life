import { Component, OnInit } from '@angular/core';

import { SocketService } from '../services/socket.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  connection;
  temperature;
  humidity;

  constructor(private socketService: SocketService) { }

  ngOnInit() {
    this.connection = this.socketService.getTemperature().subscribe(temperature => {
      this.temperature = temperature;
    });
    this.connection = this.socketService.getHumidity().subscribe( humidity => {
      this.humidity = humidity;
    });
  }

}
