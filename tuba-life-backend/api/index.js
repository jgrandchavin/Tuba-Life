const router = require('express').Router();

router.use('/temperature', require('./temperature'));
router.use('/humidity', require('./humidity'));
router.use('/reception', require('./reception'));

module.exports = router ;

