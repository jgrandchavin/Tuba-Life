const router = require('express').Router();
"use strict";

let temperature;

// GET temperature
router.get('/', (req, res) => {
    res.json({
        value: temperature
    });
});

// POST to actualize temperature
router.post('/actualize/:temp', (req, res) => {
    if (req.params.temp !== "nan"){
        temperature = req.params.temp;
    }
    res.end()
});

module.exports = router;