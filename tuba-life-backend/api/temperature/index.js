const router = require('express').Router();

router.use('/', require('./temperature'));

module.exports = router;
