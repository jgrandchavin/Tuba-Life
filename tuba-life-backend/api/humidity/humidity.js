const router = require('express').Router();

let humidity;

// GET temperature
router.get('/', (req, res) => {
    res.json({
        value: humidity
    });
});

// POST to actualize temperature
router.post('/actualize/:humidity', (req, res) => {
    if (req.params.humidity !== "nan"){
        humidity = req.params.humidity;
    }
    res.end()
});

module.exports = router;