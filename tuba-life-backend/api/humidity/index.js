const router = require('express').Router();

router.use('/', require('./humidity'));

module.exports = router;
