const router = require('express').Router();

let reception = false;

// GET temperature
router.get('/', (req, res) => {
    res.json({
        value: reception
    });
});

// POST to actualize temperature
router.post('/ring', (req, res) => {
    reception = true;
    res.end();
});


module.exports = router;