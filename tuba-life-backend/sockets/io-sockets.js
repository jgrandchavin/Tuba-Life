const request = require('request');

module.exports = function(io) {

    // Connect to the socket
    io.on('connection', (socket) => {

        const getTemperature = function() {
            let temperature;

            request('http://localhost:3000/api/temperature', { json: true }, (err, res, body) => {
                if (err) { return console.log(err); }
                temperature = body.value;
                socket.emit('temperature', temperature);
            });
        };

        const getHumidity = function () {
          let humidity;

          request('http://localhost:3000/api/humidity', { json: true }, (err, res, body) => {
             if (err) { return console.log(err);}
             humidity = body.value;
             socket.emit('humidity', humidity);
          });
        };

        // Get temperature
        setInterval(
            () => getTemperature(), 2000
        );

        setInterval(
            () => getHumidity(), 2000
        );

        socket.on("disconnect", () => {

        });

    });

};
