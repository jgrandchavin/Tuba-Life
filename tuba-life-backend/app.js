// Required Constant
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const dotEnv = require('dotenv');
dotEnv.config();

// Initialize app
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
require('./sockets/io-sockets')(io);

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(morgan('dev'));

// Port
const port = process.env.PORT || 3000;

// Route
app.use('/api', require('./api'));

// Start listening on the port
server.listen(port, function(){
  console.log(`Server started on port ${port}`);
});
